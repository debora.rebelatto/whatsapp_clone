import 'package:flutter/material.dart';
import 'chatTile.dart';

chats() {
  List<String> imageUrl = [
    "https://i.pinimg.com/originals/2e/2f/ac/2e2fac9d4a392456e511345021592dd2.jpg",
    "https://randomuser.me/api/portraits/men/86.jpg",
    "https://randomuser.me/api/portraits/women/80.jpg",
    "https://randomuser.me/api/portraits/men/43.jpg",
    "https://randomuser.me/api/portraits/women/49.jpg",
    "https://randomuser.me/api/portraits/women/45.jpg",
    "https://randomuser.me/api/portraits/women/0.jpg",
    "https://randomuser.me/api/portraits/women/1.jpg",
    "https://randomuser.me/api/portraits/men/0.jpg"
  ];
  List<bool> viewed = [ false, true, true, false, false, true, true, true ];
  List<String> time = ["9Am", "8Am", "6Am",  "Yesterday",  "San 20", "San20", "San20"];

  return Stack(children: [
    SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Container(
        padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)),
        ),
        child: Container()/* ListView.builder(
          physics: BouncingScrollPhysics(), shrinkWrap: true,
          itemBuilder: (context, index) {

            return GestureDetector(
              child: chatTile(imageUrl[index], "userName", "msg", time[index], viewed[index]),
              onTap: () {},
            );
          }
        ),*/
      )
    )
  ]);
}