import 'package:flutter/material.dart';

import 'storybutton.dart';

story() {
  List<String> imageUrl = [
    "https://i.pinimg.com/originals/2e/2f/ac/2e2fac9d4a392456e511345021592dd2.jpg",
    "https://randomuser.me/api/portraits/men/86.jpg",
    "https://randomuser.me/api/portraits/women/80.jpg",
    "https://randomuser.me/api/portraits/men/43.jpg",
    "https://randomuser.me/api/portraits/women/49.jpg",
    "https://randomuser.me/api/portraits/women/45.jpg",
    "https://randomuser.me/api/portraits/women/0.jpg",
    "https://randomuser.me/api/portraits/women/1.jpg",
    "https://randomuser.me/api/portraits/men/0.jpg"
  ];

  List<String> names = [ 'Davie', 'Jack', 'Anjie', 'Joseph', 'Juline', 'Juline', 'Juline' ];

  return Stack(children: [
    Container(
      height: 100.0,
      child: ListView.builder(
        itemCount: names.length, shrinkWrap: true, scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) => storyButton(imageUrl[index], names[index]),
      ),
    )
  ]);
}