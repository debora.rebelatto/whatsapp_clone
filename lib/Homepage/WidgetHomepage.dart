import 'package:flutter/material.dart';
import '../Chat/WidgetChat.dart';
import '../Story/WidgetStory.dart';

class Whatsapp extends StatefulWidget {
  @override
  _WhatsappState createState() => _WhatsappState();
}

class _WhatsappState extends State<Whatsapp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF177767),
      appBar: AppBar(
        elevation: 0.0, title: Text(" Clone"), backgroundColor: Color(0xFF177767),
        actions: [
          IconButton( onPressed: () {}, icon: Icon(Icons.search)),
          IconButton( onPressed: () {}, icon: Icon(Icons.more_horiz)),
        ],
      ),
      body: SingleChildScrollView(child: Column(children: [ story(), ]))
    );
  }
}
